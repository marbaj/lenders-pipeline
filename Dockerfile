FROM node:latest

RUN apt-get update
RUN apt-get install git -y
RUN mkdir app/
COPY / app/src

WORKDIR app/src

RUN rm -rf node_modules/
RUN npm install --production

ENV NODE_ENV production

EXPOSE 3000

CMD ["node", "server"]