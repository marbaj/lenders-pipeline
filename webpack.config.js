'use strict';

const path = require('path');
const loaders = require('./webpack.loaders');
const plugins = require('./webpack.plugins');
const serverConfig = require('./server/lib/config');

const HOST = process.env.HOST || "marko.ds.com";
const PORT = process.env.PORT || "8888";

module.exports = {
    entry: {
        login: ['react-hot-loader/patch', './client/login/login.js'],
        admin: ['react-hot-loader/patch', './client/admin/index.js'],
        lender: ['react-hot-loader/patch', './client/lender/index.js'],
        'app-styles': ['./assets/sass/app.scss'],
        'admin-app-styles': ['./assets/sass/admin-app.scss'],
    //    bootstrap: ['./node_modules/bootstrap/scss/bootstrap.scss']
    },

    output: {
        publicPath: '/public',
        path: path.join(__dirname, serverConfig.get("buildFolder")),
        filename: '[name].js'
    },

    resolve: { extensions: ['.js', '.jsx', 'scss'] },

    module: { loaders },

    devServer: {
        noInfo: true,
        hot: true,
        inline: true,
        historyApiFallback: true,
        port: PORT,
        host: HOST,
        proxy: { "**": `http://localhost:${serverConfig.get("port")}` }   
    },

    plugins: plugins,
  
    devtool: 'source-map'
};
