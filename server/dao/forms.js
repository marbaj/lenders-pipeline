const sequelize = require('../dao/sql-connect');
const { Form, FormField } = require('../models/form');
const { Op } = require('sequelize');
const { hashid } = require('../lib/security');
const lodash = require('lodash');

/**
 * 
 * @param {*} data 
 */
const serializeForm = data => {

    if (!Array.isArray(data)) {
        throw Error('Form input filelds has to be an Array');
    }

    const elements = (element, row) => {
        element = Array.isArray(element) ? element : [ element ];

        return element.map((data, col) => {
            return {
                name: data.name,
                label: data.label,
                placeholder: data.placeholder,
                type: data.type,
                row: row,
                col: col
            };
        });
    };

    const list = data.map(elements);

    return list.reduce((a,b) => { 
        return a.concat(b) 
    }, []);
};

/**
 * 
 * @param {*} data 
 */
const deserializeForm = data => {
    const form = {
        name: data.name,
        id: hashid.encode(data.id)
    };

    form.inputs = (data.formFields || []).reduce((fields, value) => {
        const field = {
            id: hashid.encode(value.id),
            label: value.label,
            placeholder: value.placeholder,
            type: value.type
        };

        if (fields[value.row] === undefined) {
            fields.push([field]);
        } else {
            fields[value.row].push([field]);
        }

        return fields;

    }, []);

    return form;
};

module.exports.createForm = async data => {
    try {
        const formFields = serializeForm(data.inputs);

        const formDbObj = await Form.create({
            name: data.name
        });
    
        const formFieldsDboj = await FormField.bulkCreate(formFields);
        
        return await formDbObj.addFormField(formFieldsDboj);
    } catch (e) {
        console.log(e);
        throw new Error(e);
    }
};

/**
 * Retreive all forms
 */
module.exports.getAllForms = async () => {
    const fomrObjs = await Form.findAll();

    const forms = fomrObjs.map(f => {
        return {
            id: hashid.encode(f.id),
            name: f.name
        };
    })

    return forms;
};

module.exports.getFormById = async id => {
    try {
        const formDbObj = await Form.find({
            where: {
                id: { [Op.eq]: id }
            },
            include: [{
                model: FormField
            }]
          });

          return deserializeForm(formDbObj);
    } catch (e) {
        throw new Error(e);
    }
};

/**
 * 
 * @param {*} org 
 */
const getByUserId = async org => {
    try {
        const data = await Organization.findAll();
        return senatizedOrg(data);
    } catch (error) {
        throw error;
    }
};

