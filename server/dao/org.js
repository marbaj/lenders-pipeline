'use strict';

const sequelize = require('../dao/sql-connect');
const { Organization } = require('../models');
const { hashid } = require('../lib/security');

/**
 * 
 * @param {*} data 
 */
const orgObject = data => {
    return {
        id: hashid.encode(data.id),
        name: data.name,
        org: data.org
    };
};

/**
 * 
 * @param {*} data 
 */
const senatizedOrg = data => {
    const t = obj => orgObject(obj.dataValues)
    return Array.isArray(data) ? data.map(d => t(d)) : t(data);
};

/**
 * 
 * @param {*} org 
 */
const create = async org => {
    try {
        const data = await Organization.create(org);
        return senatizedOrg(data);
    } catch (err) {
        throw err;
    }
};

/**
 * 
 * @param {*} org 
 */
const getAll = async org => {
    try {
        const data = await Organization.findAll();
        return senatizedOrg(data);
    } catch (error) {
        throw error;
    }
};

/**
 * 
 * @param {*} org 
 */
const getByUserId = async org => {
    try {
        const data = await Organization.findAll();
        return senatizedOrg(data);
    } catch (error) {
        throw error;
    }
};

module.exports = {
    createOrg: create,
    getAllOrgs: getAll,
    getOrgByUserId: getByUserId
};
