'use strict';

const Sequelize = require('sequelize');
const config = require('../lib/config');
const logger = require('../lib/logger');

const host = config.get('sql:host');
const dialect = config.get('sql:dialect');
const db = config.get('sql:db');
const user = config.get('sql:user');
const pass = config.get('sql:password');

const sequelize = new Sequelize(db, user, pass, {
    host: host,
    dialect: dialect,
  
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    }
});

sequelize
.authenticate()
.then(() => {
    logger.info('Connection has been established successfully.');
})
.catch(err => {
    logger.debug('Unable to connect to the database:', err);
});

module.exports = sequelize;
