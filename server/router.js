'use strict';

const path = require('path');
const passport = require('passport');
const config = require('./lib/config');
const logger = require('./lib/logger');
const userController = require('./controllers/users-controller');
const loginController = require('./controllers/login-controller');
const orgController = require('./controllers/org-controller');
const formController  = require('./controllers/form-controller');
const outFolder = config.get('buildFolder');
const { ADMIN_DOMAIN, ORG_DOMAIN, USER_DOMAIN } = require('./dao/domains');

const auth = (req, res, next) => { 
    if (req.isAuthenticated()) {
        next();
    } else {
        res.redirect('/login');
    }
};

const router = app => {
    
    app.use((req, res, next) => {
        const host = req.host;
        const domain = config.get('domain');
        const n = host.indexOf(`.${domain}`);
        const rest = host.substring(0, n);
        const parts = rest.split('.').reverse();
        const len = parts.length;
        
        // routing rules
        if (len === 0) {
            res.redirect(`/wellcome.html`);
        } else if (len === 1) {
            if (!parts[0] || parts[0] === 'www' || parts[0] === '') {
                res.redirect(`/wellcome.html`);
            } else {
                req.org = parts[0];
                next();
            }
        } else if (len > 1) {
            res.redirect(`/error.html`);
        }

        // request logger
        logger.info( req.host);
    });

    app.use('/login', loginController);

    app.get('/logout', (req, res) => {
        logger.info(`Logout User: ${req.user.user.username}`);
        req.logout();
        res.redirect('/');
    });

    app.use('/users', auth, userController);
    app.use('/org', auth, orgController);
    app.use('/forms', auth, formController);

    app.get('/*', auth, (req, res) => {
        let site, style;

        switch (req.user.user.domain) {
            case ADMIN_DOMAIN:
                site = 'admin';
                style = 'admin'
                break;
            case ORG_DOMAIN:
                site = 'lender';
                style = 'admin'
                break;
            case USER_DOMAIN:
                site = 'client';
                style = 'admin'
                break;
            default:
                res.redirect(`/login`);
                return;
        }

        logger.info(`Main client app page served for User: ${req.user.user.username}`);

        res.render('index', { title: 'Admin App', style: style, app: site });
    });
};

module.exports = router;
