'use strict';

const config = require('../lib/config');
const logger = require('../lib/logger');
const express = require('express');
const router = express.Router();
const { getAllUsers, createUser, userById, userRoles, userObject, updateUser } = require('../user-service');
const { createOrg, getAllOrgs } = require('../dao/org');
const { ADMIN, USER } = require('../dao/roles');
const { ORG_DOMAIN, CLIENT_DOMAIN, ADMIN_DOMAIN } = require('../dao/domains');
const { roleAuth } = require('./security');
const { hashid } = require('../lib/security');

router.get('/all', roleAuth(ADMIN), async (req, res) => {
    const orgs = await getAllOrgs();
 
    res.json({ orgs: orgs });
});

router.post('/create', roleAuth(ADMIN), async (req, res) => {
    const data = req.body;

    // get user's org
    const parentId = req.user.org.id;

    const org = {
        name: data.name,
        parentId: parentId,
        org: data.org,
        domain: ORG_DOMAIN
    };

    const user = {
        username: data.org_admin_email,
        email: data.org_admin_email,
        password: data.org_admin_pass,
        domain: ORG_DOMAIN
    };

    try {
        const orgObject = await createOrg(org);
        await createUser(user, [ ADMIN ], orgObject.org);
        res.json(orgObject);
    } catch (error) {
        res.sendStatus(500);
    }
});

router.post('/create/clinet-org', roleAuth(ADMIN), async (req, res) => {
    const data = req.body;

     // get user's org
     const parentId = req.user.org.id;     

    const org = {
        name: data.name,
        parentId: parentId,
        org: data.org,
        domain: CLIENT_DOMAIN
    };

    try {
        const orgObject = await createOrg(org);
        res.json(orgObject);
    } catch (error) {
        res.sendStatus(500);
    }
});

module.exports = router;