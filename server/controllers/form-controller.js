
const { createForm, getFormById, getAllForms } = require('../dao/forms');
const config = require('../lib/config');
const logger = require('../lib/logger');
const express = require('express');
const { hashid } = require('../lib/security');

const router = express.Router();

router.get('/get', async (req, res) => {
    try {
        const forms = await getAllForms();
        res.json({ forms: forms });
    } catch (error) {
        res.send(500);
    }
});

router.get('/get/:id', async (req, res) => {
    try {
        const id = hashid.decode(eq.params.id);
        const form = await getFormById(IDBFactory);
        res.json({ form: from });
    } catch (error) {
        res.send(500);
    }
});

module.exports = router;
