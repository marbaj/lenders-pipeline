'use strict';

const path = require('path');
const config = require('../lib/config');
const logger = require('../lib/logger');
const express = require('express');
const passport = require('passport');
const router = express.Router();

const outFolder = config.get('buildFolder');

router.get('/', (req, res) => {
    if (req.isAuthenticated()) {
        res.redirect('/');
    } else {
        logger.info('login page served');
        res.render('login', { title: 'Admin App Login' });
    }
});

/**
 * Authorize user credentials
 */
router.post('/', (req, res, next) => {
    passport.authenticate('local', (err, user, info) => {
        if (err) { 
            return next(err); 
        } else if (!user) { 
            return res.redirect('/login'); 
        }

        req.logIn(user, err => err ? next(err) : res.redirect('/'));
    })(req, res, next);
});

module.exports = router;

