'use strict';

const { userRoles } = require('../user-service');

/**
 * 
 */
const roleAuth = (...allowedRoles) => {

    const allowed  = allowedRoles || [];

    return async (req, res, next) => {
        try {
            const roles = await userRoles(req.user.user.id);
            const isAllowed = roles.some(role => allowed.find(a => a === role.role));

            if (isAllowed) {
                next();
            } else {
                res.status(403).json({ message: "Forbidden" });
            }
        } catch (error) {
            res.status(403).json({ message: "Forbidden" });
        }
    };
};

module.exports = {
    roleAuth: roleAuth
};