'use strict';

const config = require('../lib/config');
const logger = require('../lib/logger');
const express = require('express');
const router = express.Router();
const { getAllUsers, createUser, userById, userRoles, userObject, updateUser, updatePassword } = require('../user-service');
const { ADMIN, USER } = require('../dao/roles');
const { roleAuth } = require('./security');
const { hashid } = require('../lib/security');

const senatizeUser = (user, org) => {
    return {
        id: hashid.encode(user.id),
        username: user.userName,
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email
    }; 
};

const senatizeOrg = (org) => {
    return  {
        name: org.name,
        id: hashid.encode(org.id),
    };
};

router.get('/info', async (req, res) => {
    const info = {
        user: senatizeUser(req.user.user),
        org: senatizeOrg(req.user.org)
    };

    res.json({ info: info });
});

router.get('/get-all', roleAuth(ADMIN), async (req, res) => {   
    try {    
        const org = req.user.org.org;
        const dbUsers = await getAllUsers(org);

        const users = dbUsers.map(user => {
            user.id = hashid.encode(user.id);
            return user;
        });

        res.json({ users: users });
    } catch (error) {
        logger.error(error);
        res.sendStatus(500);
    }
});

router.get('/get/:id', roleAuth(ADMIN), async (req, res) => {   
    try {
        const id = hashid.decode(req.params.id)[0];
        const user = await userById(id);
        const roles = ''//await userRoles(id);
        res.json({ user: userObject(user), roles: roles });
    } catch (error) {
        res.sendStatus(500);
    }
});

router.post('/create', roleAuth(ADMIN), async (req, res) => {
    try {
        const data = {
            user:  req.bod,
            password: req.body.password,
            domain: req.user.domain
        };

        await createUser(user);
        res.sendStatus(200);
    } catch (error) {
        res.sendStatus(500);
    }
});

router.put('/update', async (req, res) => {
    try {
        const user = req.body;
        await updateUser(req.user.user.id, user);
        req.user.user = await userById(req.user.user.id);

        const info = {
            user: senatizeUser(req.user.user),
            org: senatizeOrg(req.user.org)
        };

        req.login(req.user, (err) => {
            res.json({ info: info });
        });
    } catch (error) {
        res.sendStatus(500);
    }
});

router.put('/update/password', async (req, res) => {
    try {
        const data = req.body;
        const updatedUser = await updatePassword(req.user.user.id, data.password_old, data.password_new);
        res.sendStatus(200);
    } catch (error) {
        res.sendStatus(500);
    }
});

router.put('/update/:id', roleAuth(ADMIN), async (req, res) => {
    try {
        const user = req.body;
        const id = hashid.decode(req.params.id)[0];
        const updatedUser = await updateUser(id, user);
        res.json(updatedUser);
    } catch (error) {
        res.sendStatus(500);
    }
});

module.exports = router;
