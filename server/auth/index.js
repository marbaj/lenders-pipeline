'use strict';

const Strategy = require('passport-strategy').Strategy;
const { authUser } = require('../user-service');
const passport = require('passport');
const logger = require('../lib/logger');

const Auth = class extends Strategy {
    
    constructor (options = {}) {
        super(options);
    };

    async authenticate (req, options) {

        const authFailed = reason => {
            logger.info('authentication failed: ', reason);
            this.fail({});
        };

        try {
            const user = req.body.username;
            const pass = req.body.password;
            const org = req.org;
            
            const info = await authUser(user, pass, org);

            if (info) {
                this.success(info);
            } else {
                authFailed('user object problem')
            }
        } catch (error) {
            authFailed(error);
        }
    };
};

passport.serializeUser((user, done) => {
    done(null, user);
});

passport.deserializeUser((user, done) => {
    done(null, user);
});

passport.use('local', new Auth());
