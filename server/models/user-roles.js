'use strict';

const sequelize = require('../dao/sql-connect');
const User = require('./user');
const { STRING, INTEGER, Deferrable } = require('sequelize');

const Role = sequelize.define('roles', {
    id: {
        type: INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    role: {
        type: STRING,
        allowNull: false
    }
});

module.exports = Role;
