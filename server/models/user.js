'use strict';

const sequelize = require('../dao/sql-connect');
const Role = require('./user-roles');
const Oganization = require('./organization');
const { STRING, INTEGER, Deferrable } = require('sequelize');

const User = sequelize.define('user', { 
    id: {
        type: INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    firstName: {
        type: STRING
    },
    lastName: {
        type: STRING
    },
    username: {
        type: STRING,
        allowNull: false,
    },
    email: {
        type: STRING,
        allowNull: false,
    },
    password: {
        type: STRING,
        allowNull: false
    },
    domain: {
        type: STRING,
        allowNull: false,
    }
});

/**
 * Make association between User and Roles
 */
User.hasMany(Role, { as: 'roles' });

/**
 * Make association between User and Organization
 */
User.belongsTo(Oganization, { foreignKey: 'org', targetKey: 'org' });

sequelize.sync();

module.exports = User;
