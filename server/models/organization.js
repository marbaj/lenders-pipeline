'use strict';

const sequelize  = require('../dao/sql-connect');
const { STRING, INTEGER } = require('sequelize');
const name = 'organizations';

const Org = sequelize.define(name, {
    id: {
        type: INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    parentId: {
        type: INTEGER
    },
    name: {
        type: STRING,
        allowNull: false
    },
    org: {
        type: STRING,
        allowNull: false,
        unique: true
    },
    domain: {
        type: STRING,
        allowNull: false,
    }
});

module.exports = Org;
