'use strict';

const sequelize  = require('../dao/sql-connect');
const { STRING, INTEGER } = require('sequelize');

const formName = 'form';
const formFieldName = 'formField'

const Form = sequelize.define(formName, {
    id: {
        type: INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: STRING
    }
});

const FormField =sequelize.define(formFieldName, {
    id: {
        type: INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    row: {
        type: INTEGER
    },
    col: {
        type: INTEGER
    },
    label: {
        type: STRING,
    },
    placeholder: {
        type: STRING
    },
    type: {
        type: STRING
    }
});

/**
 * Make association between Form and FormFields
 */
Form.hasMany(FormField);

sequelize.sync();

module.exports = { Form: Form, FormField: FormField };
