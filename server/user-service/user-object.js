'use strict';

module.exports = data => {
    const obj = {
        id: data.id,
        username: data.username,
        firstName: data.firstName,
        lastName: data.lastName,
        email: data.email,
        domain: data.domain
    };

    return obj;
};