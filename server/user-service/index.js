'use strict';

const { createUser, authUser, getAllUsers, userById, userRoles, updateUser, updatePassword } = require('./user-dao');

module.exports = {
    createUser: createUser,
    authUser: authUser,
    getAllUsers: getAllUsers,
    userRoles: userRoles,
    userById: userById,
    updateUser: updateUser,
    updatePassword, updatePassword,
    userObject: require('./user-object')
};
