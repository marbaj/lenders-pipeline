'use strict';

const { createPass, comparePass } = require('./password');
const logger = require('../lib/logger');
const { Op } = require('sequelize');
const { User, Role, Organization } = require('../models');
const { USER } = require('../dao/roles');
const userObject = require('./user-object');

/**
 * 
 * @param {*} data 
 */
const senatizedUser = data => {
    const t = obj => userObject(obj.dataValues);
    return Array.isArray(data) ? data.map(d => t(d)) : t(data);
};

/**
 * Query records by record Id
 * @param {*} id Object id
 * @return {Object} Returns query object
 */
const queryById = id => {
    return {
        where: {
            id: { [Op.eq]: id }
        }
    };
};

/**
 * 
 * @param {*} user 
 * @param {*} roles 
 * @param {*} orgId 
 */
const createUser = async (user, roles, orgName) => {
    const email = user.email ? user.email : user.username;
    const username = user.username ? user.username : user.email;
    
    try {
        logger.info('creating user: ', username);
        const hashPass = await createPass(user.password);

        const userDbo = await User.create({
            username: username,
            email: email,
            password: hashPass,
            domain: user.domain,
            firstName: user.firstName,
            lastName: user.lastName,
            org: orgName
        });

        roles = (roles || [ USER ]).map(role => {
            return { role: role };
        });

        const rolesDbo = await Role.bulkCreate(roles);
        
        await userDbo.addRoles(rolesDbo);

        return userDbo;
    } catch (error) {
        logger.error(error);
        throw error;
    }
};

/**
 * Authenticate User
 * @param {String} username Username
 * @param {String} pass Password
 * @param {String} org Organization
 */
const authUser = async (username, pass, org) => {
    try {
        const user = await User.find({
            where: { org: org },
            include: [{
                model: Organization,
                attributes: [ 'id', 'name', 'org' ]
            }]
          });

        if (!user || user.username !== username) {
            throw new Error('User object not found');
        }

        const isAuth = await comparePass(pass, user.password);
        if (isAuth) {
            return {
                user: senatizedUser(user),
                org: user.dataValues.organization.dataValues
            };
        } else {
            throw new Error('User auth failed');
        }
    } catch (error) {
        throw error;
    }  
};

/**
 * 
 */
const getAllUsers = async (org) => {
    try { 
        const query = {
            where: {
                org: { [Op.eq]: org }
            }
        };
    
        const users = await User.findAll(query);
        return senatizedUser(users);
    } catch (error) {
        throw error;
    }
};

/**
 * 
 * @param {*} userId 
 */
const userRoles = async userId => {
    const query = {
        attributes: [ 'role' ],
        where: {
            userId: userId
        }
    };

    try {
        return await Role.findAll(query);
    } catch (error) {
        throw (error);
    }
};

const userById = async id => {
    try {
        const user = await User.findById(id);
        return senatizedUser(user);
    } catch (error) {
        throw error;
    }
};

const updateUser = async (id, user) => {
    try {
         // make sure password is not updated
        delete user.password;
        delete user.id;
        const query = queryById(id);
        return await User.update(user, query);
    } catch (error) {
        throw error;
    }
};

const updatePassword = async (id, oldPass, newPass) => {
    try {
        const user = await User.findById(id);
        const flag = await comparePass(oldPass, user.password);
        if (flag) {
            const query = queryById(id);
            const hashPass = await createPass(newPass);
            await User.update({ password: hashPass }, query);

            return true;
        }
    } catch (error) {
        throw error;
    }
}

module.exports = {
    createUser: createUser,
    authUser: authUser,
    getAllUsers: getAllUsers,
    userRoles: userRoles,
    userById: userById,
    updateUser: updateUser,
    updatePassword: updatePassword
};
