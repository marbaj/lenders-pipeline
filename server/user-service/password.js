'use strict';

const bcrypt = require('bcrypt');

const saltRounds = 10;

module.exports = {
    createPass (pass) {
        return bcrypt.hash(pass, saltRounds); 
    },

    comparePass (pass, hash) {
        return bcrypt.compare(pass, hash);
    }
    
};