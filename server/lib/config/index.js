'use strict';

const nconf = require('nconf');
const store = new nconf.Provider();

store.argv().env();

const loadConf = (type) => {
    const file = `${process.cwd()}/server/properties/config-${type}.json`;
    store.file(type, file);   
};

if (store.get('NODE_ENV') === 'production') {
    loadConf('production');
}

loadConf('default');

module.exports = store;