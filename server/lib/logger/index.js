'use strict';

const { createLogger, format, transports } = require('winston');
const fs = require('fs');

const logDir = 'logs';
const env = process.env.NODE_ENV;
const tsFormat = () => (new Date()).toLocaleTimeString();

try { fs.mkdirSync(`${process.cwd()}/${logDir}`); } catch (err) { }

const logger = createLogger({
    format: format.simple(),
    transports: [
        new (transports.Console)({ 
            colorize: true,
            timestamp: tsFormat
        }),
        new (transports.File)({  //require('winston-daily-rotate-file')
          filename: `${logDir}/system.log`,
          timestamp: tsFormat,
          datePattern: 'yyyy-MM-dd',
          level: env === 'development' ? 'debug' : 'info'
        })

     //   new (transports.File)({ filename: `${dir}/error.log`, level: 'error' }),
     //   new (transports.File)({ filename: `${dir}/system.log`,level: 'info' })
    ]
});

logger.level = 'debug';
logger.debug('Debugging info');
logger.verbose('Verbose info');
logger.info('Info');
logger.warn('Warning message');
logger.error('Error info');

logger.exceptions.handle(
    new transports.File({ filename: `${logDir}/exceptions.log` })
);

// if (process.env.NODE_ENV !== 'production') {
//     logger.add(new transports.Console({
//     //    format: format.simple(),
//         level: 'info',
//         formatter: function(options) {
//             return "Msrko";
//             // - Return string will be passed to logger.
//             // - Optionally, use options.colorize(options.level, <string>) to
//             //   colorize output based on the log level.
//             return options.timestamp() + ' ' +
//               config.colorize(options.level, options.level.toUpperCase()) + ' ' +
//               (options.message ? options.message : '') +
//               (options.meta && Object.keys(options.meta).length ? '\n\t'+ JSON.stringify(options.meta) : '' );
//           }
//     }));
// }

module.exports = logger;
