'use strict';

const Hashids = require('hashids');
const config = require('../config');

const salt = config.get('hashid');
const hashLen = 10;

module.exports.hashid = new Hashids(salt, hashLen);
