'use strict';

const logger = require('./lib/logger');

process.on('uncaughtException', err => {
    console.log(err)
    logger.error(err); 
});

process.on('unhandledRejection', (reason, p) => {
    console.log(reason)
    logger.error(reason);
});

// class CustomError extends Error {
//     constructor(foo = 'bar', ...params) {
//       // Pass remaining arguments (including vendor specific ones) to parent constructor
//       super(...params);
  
//       // Maintains proper stack trace for where our error was thrown
//       Error.captureStackTrace(this, CustomError);
  
//       // Custom debugging information
//       this.foo = foo;
//       this.date = new Date();
//     }
// };

// return CustomError;
  