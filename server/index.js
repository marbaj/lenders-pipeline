'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const config = require('./lib/config');
const logger = require('./lib/logger');
const outFolder = config.get('buildFolder');
const passport = require('passport');;
const session = require('express-session');
const RedisStore = require('connect-redis')(session);
const path = require('path');
const pub = require('pug');

require('./error');

const app = express();

app.use(bodyParser.json());

app.set('view engine', 'pug')

const port = config.get('port');

const sessionOptions = {
    secret: config.get('session:secret'),
    resave: false,
    saveUninitialized: false,
    httpOnly: true,
    name: config.get('session:name')
};

if (config.get('redis:enabled')) {
    sessionOptions.store =  new RedisStore({
        host: config.get('redis:host'),
        port: config.get('redis:port')
    });
}

app.use(session(sessionOptions));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.resolve(__dirname, '..', outFolder)));

require('./auth');
require('./router')(app);

logger.info('test Marko')

app.listen(port, () => logger.info(`App started and listening on port ${port}`));
  
module.exports = app;
