'use strict';

require('./error');

const { createOrg } = require('./dao/org');
const { createUser } = require('./user-service');
const { connection } = require('./dao')
const { ADMIN } = require('./dao/roles');
const { ADMIN_DOMAIN } = require('./dao/domains');
const logger = require('./lib/logger');

const init = async (userName, pass) => {
    logger.info('init database');
    try {
        const orgObject = await createOrg({ 
            name: 'lenders-pipeline',
            org: 'admin',
            parentId: 0,
            domain: ADMIN_DOMAIN
        });
        
        const user = {
            username: userName,
            password: pass,
            domain: ADMIN_DOMAIN
        };
         
        await createUser(user, [ ADMIN ], orgObject.org);
    }  catch (error) {
        logger.error(error);
    } finally {
        return connection.close();
    }
};

init('admin', 'admin');
