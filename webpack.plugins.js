'use strict';

const webpack = require('webpack');
const DashboardPlugin = require('webpack-dashboard/plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = [

    new webpack.NoEmitOnErrorsPlugin(),
    
    new webpack.NamedModulesPlugin(),

    new webpack.HotModuleReplacementPlugin(),

    new DashboardPlugin(),

    // new HtmlWebpackPlugin({
    //     template: './public/index.html',
    //     chunks: ['login'],
    //     filename: 'login.html',
    //     inject: 'body'
    // }),

    // new HtmlWebpackPlugin({
    //     template: './public/index.html',
    //     chunks: ['admin'],
    //     filename: 'admin.html',
    //     inject: 'body'
    // }),

    // new HtmlWebpackPlugin({
    //     template: './public/index.html',
    //     chunks: ['lender'],
    //     filename: 'lender.html',
    //     inject: 'body'
    // }),

    new HtmlWebpackPlugin({
        template: './public/wellcome.html',
        chunks: [''],
        filename: 'wellcome.html',
        inject: 'body'
    }),

    new HtmlWebpackPlugin({
        template: './public/error.html',
        chunks: [''],
        filename: 'error.html',
        inject: 'body'
    }),

    new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery',
        Popper: ['popper.js', 'default']
    }),

    new ExtractTextPlugin({ 
        // define where to save the file
        filename: 'css/[name].css',
        allChunks: true
    }),
];