import { createStore, applyMiddleware } from 'redux';
import allReducers from './reducers';
import thunk from 'redux-thunk';
import loggedInUser from './actions';
import axios from 'axios';

const store = createStore(
    allReducers,
    applyMiddleware(thunk)
);

const loadData = () => {
    
     return (dispatch) => {
         axios.get('/users/info')
         .then(res => {
             return dispatch(loggedInUser({ type: 'LOGGEDIN_USER', org: res.data.info.org, user: res.data.info.user }));
         })
        .catch (() => {
        })
     };
};
