import React, { Component } from 'react';
import { connect } from 'react-redux';
import Navbar from '../components/navbar';
import { InputForm } from '../components/MBForm';
import { Link } from 'react-router-dom';
import userForm from '../forms/user-edit.json';
import populeteForm from '../lib/populete-form';

class User extends Component {

  constructor (props) {
      super(props);  
      this.items = userForm;
      this.items.static = false;
      this.items.submit = data => this.updateUser(data);
      this.items.cancel = () => this.onCancel();
      this.onCancel = this.onCancel.bind(this);
  };

  updateUser (user) {
    const userId = this.props.match.params.id;
    axios.put(`/users/update/${userId}`, user)
    .then(response => this.onCancel())
    .catch(error => this.onCancel());
  };

  onCancel () {
    this.props.history.push(`/users/${this.props.match.params.id}`);
  };

  render () {
    let form;
    if (this.props.users) {
      const user = (this.props.users || []).find(user => user.id === this.props.match.params.id);
      form = populeteForm(user, this.items);
    }

    return (
      <div>
        <Navbar/>
        <InputForm form={form}/>
      </div>
    );
  }
};

const mapStateToProps = (state) => {
  return {
    users: state.users
  };
};

export default connect(mapStateToProps)(User);

