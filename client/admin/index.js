import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import allReducers from '../reducers';
import thunk from 'redux-thunk';
import { loadUserData } from '../data';

const store = createStore(
    allReducers,
    applyMiddleware(thunk)
);

store.dispatch(loadUserData());

ReactDOM.render(
    <Provider store={store}> 
        <App/>
    </Provider>, 
    document.getElementById('root')
);
