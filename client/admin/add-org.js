import React, { Component } from 'react';
import Navbar from '../components/navbar';
import axios from 'axios';
import { InputForm }  from '../components/MBForm';
import form from '../forms/org.json';
import { newOrg } from '../actions';
import { connect } from 'react-redux';
import { loadAllOrgs } from '../data';

const backPath = '/orgs';

class AddOrg extends Component {

    constructor (props) {
        super(props);

        form.submit = data => this.create(data);
        form.cancel = () => this.props.history.push(backPath);
        this.state = { form: form };
        this.onStatic = this.onStatic.bind(this);
  };

  componentDidMount () {
    if (!this.props.orgs || this.props.orgs.length === 0) {
      this.props.dispatch(loadAllOrgs());
    }
  };

  create (data) {
    const createOrg = org => {
      return dispatch => {
        axios.post('/org/create', org)
        .then(res => {
          dispatch(newOrg(res.data)) ;
          this.props.history.push(backPath);
        })
        .catch (() => { });
      };
    };
    
    this.props.dispatch(createOrg(data));
  };

  onStatic () {
    this.state.form.static = !this.state.form.static;
    this.setState({ form: this.state.form })
  };

  render () {
    return (
      <div>
        <Navbar/>
        <InputForm form={this.state.form}/>
      </div>
    );
  }
};

export default connect()(AddOrg);