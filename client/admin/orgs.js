import React, { Component } from 'react';
import Navbar from '../components/navbar';
import List from '../components/List';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { loadAllOrgs } from '../data';

class Orgs extends Component {

  constructor (props) {
      super(props);
  };

  componentDidMount () {
    if (!this.props.orgs || this.props.orgs.length === 0) {
      this.props.dispatch(loadAllOrgs());
    }
  };

  render () {
    return (
      <div>
        <Navbar/>
        <div className="container mt-5">
          <div className="row justify-content-center mb-3">
            <div className="col-8">
            <Link className="btn btn-outline-primary btn-sm" role="button" to='/org/add'>Add Organization</Link>
            </div>
          </div>
          <div className="row justify-content-center">
            <div className="col-8">
              <List items={this.props.orgs}/>
            </div>
          </div>   
        </div>
      </div>
    );
  }
};

const mapStateToProps = (state) => {
  let orgs;
  if (state.orgs) {
    orgs = (state.orgs || []).map(org => {
      return { name: org.name, link: `/org/${org.id}` };
    });    
  }

  return { orgs: orgs };
};

export default connect(mapStateToProps)(Orgs);