import React, { Component } from 'react';
import Navbar from '../components/navbar';
import axios from 'axios';
import { ViewForm } from '../components/MBForm';
import { Link } from 'react-router-dom';
import userForm from '../forms/user-profile.json';
import populeteForm from '../lib/populete-form';
import { connect } from 'react-redux';
import { loadAllUsers } from '../data';

class User extends Component {

  constructor (props) {
      super(props);  

      this.items = userForm;
      this.items.static = true;
      this.userId = this.props.match.params.id;
  };

  componentDidMount () {
    if (!this.props.users || this.props.users.length === 0) {
      this.props.dispatch(loadAllUsers());
    }
  };

  render () {
    
    let form;
    if (this.props.users) {
      const user = (this.props.users || []).find(user => user.id === this.userId);
      form = populeteForm(user, this.items);
    }

    const editButton = () => {
      return (<Link className="btn btn-outline-primary btn-sm" to={`/users/${this.userId}/edit`}>EDIT</Link>);
    };

    const deleteButton = () => {
      return (<Link className="btn btn-outline-primary btn-sm" to={`/users/${this.userId}/delete`}>DELETE</Link>);
    };

    return (
      <div>
        <Navbar/>
        <ViewForm form={form}>
          <div className="row">
            <div className="mr-3">{(editButton())}</div>
            <div className="float-right">{(deleteButton())}</div>
          </div>
        </ViewForm>
      </div>
    );
  }
};

const mapStateToProps = (state) => {
  return {
    users: state.users,
    loggedInUser: state.userInfo ? state.userInfo.user : null
  };
};

export default connect(mapStateToProps)(User);
