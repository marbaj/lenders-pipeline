import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Navbar from '../components/navbar';
import { connect } from 'react-redux';

class Main extends Component {

  constructor (props) {
      super(props);
  };

  render () {
    return (
      <div>
        <Navbar />
        <div className="row justify-content-md-center justify-content-lg-center mt-5 mb-3">
          <div className="col-6">
            <Link className="btn btn-primary btn-lg btn-block" role="button" to='/users'>Users</Link>  
          </div>
        </div>
        <div className="row justify-content-md-center justify-content-lg-center mt-3">
          <div className="col-6">
            <Link className="btn btn-primary btn-lg btn-block" role="button" to='/orgs'>Organizations</Link>  
          </div>
        </div>
      </div>
    );
  }
};

const mapStateToProps = (state) => {
  return {
    userInfo: state.userInfo
  };
};

export default connect(mapStateToProps)(Main);