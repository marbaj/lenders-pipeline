import React, { Component } from 'react';
import Navbar from '../components/navbar';
import { InputForm } from '../components/MBForm';
import { Link } from 'react-router-dom';
import orgForm from '../forms/org-edit.json';
import populeteForm from '../lib/populete-form';
import { connect } from 'react-redux';
import { loadAllOrgs } from '../data';

class Org extends Component {

  constructor (props) {
      super(props);  

      this.items = orgForm;
      this.items.static = true;
  };

  componentDidMount () {
    if (!this.props.orgs || this.props.orgs.length === 0) {
      this.props.dispatch(loadAllOrgs());
    }
  };

  render () {
    const editButton = () => {
      return (<Link className="btn btn-outline-primary btn-sm" to={`/users/${this.userId}/edit`}>EDIT</Link>);
    };

    let form;
    if (this.props.orgs) {
      const org = (this.props.orgs || []).find(org => org.id === this.props.match.params.id);
      form = populeteForm(org, this.items);
    }

    return (
      <div>
        <Navbar/>
        <InputForm form={form}>
          <div className="row">
            <div className="mr-3">{(editButton())}</div>
          </div>
        </InputForm>
      </div>
    );
  }
};

const mapStateToProps = (state) => {
  return {
    orgs: state.orgs,
  };
};

export default connect(mapStateToProps)(Org);

