import React, { Component } from 'react';
import Navbar from '../components/navbar';
import axios from 'axios';
import { InputForm } from '../components/MBForm';
import { Link } from 'react-router-dom';
import orgForm from '../forms/user-edit.json';
import populeteForm from '../lib/populete-form';

class User extends Component {

  constructor (props) {
      super(props);  

      this.items = userForm;
      this.items.static = true;
      this.items.submit = data => this.updateUser(data)
      this.items.cancel = data => this.onStatic();

      this.state = { };

      this.onStatic = this.onStatic.bind(this);
      this.applyModel = this.applyModel.bind(this);
  };

  applyModel (model) {
    const form = populeteForm(model.user, this.items);
    this.setState({ form: form });
  };

  componentDidMount () {
    const userId = this.props.match.params.id;
    axios.get(`/users/get/${userId}`)
    .then(response => this.applyModel(response.data))
    .catch(error => { });
  };

  updateUser (user) {
    const userId = this.props.match.params.id;
    axios.put(`/users/update/${userId}`, user)
    .then(response => this.onStatic())
    .catch(error => this.onStatic());
  };

  onStatic () {
    this.state.form.static = !this.state.form.static;
    this.setState({ form: this.state.form });
  };

  render () {

    const editButton = () => {
      if (this.state.form && this.state.form.static) {
        return (<button className="btn btn-outline-primary btn-sm" onClick={this.onStatic}>EDIT</button>);
      } else {
        return (<div/>);
      }
    };

    return (
      <div>
        <Navbar/>
        <InputForm form={this.state.form}>
          {(editButton())}
        </InputForm>
      </div>
    );
  }
};

export default User;
