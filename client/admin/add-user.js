import React, { Component } from 'react';
import Navbar from '../components/navbar';
import axios from 'axios';
import { InputForm } from '../components/MBForm';
import userForm from '../forms/users.json';
import { connect } from 'react-redux';

class User extends Component {

  constructor (props) {
      super(props);

      userForm.submit = (data) => this.createUser(data);
      userForm.cancel = () => this.props.history.push('/users');

      this.state = {
        form: userForm
      };
  };

  createUser (user) {
    axios.post('/users/create', user)
    .then(response => {
    })
    .catch(error => {
    })
    .finally (() => this.props.history.push('/users'))
  };

  render () {
    return (
      <div>
        <Navbar/>
        <InputForm form={this.state.form}/>
      </div>
    );
  }
};
export default connect()(User);
