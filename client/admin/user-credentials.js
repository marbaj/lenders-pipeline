import React, { Component } from 'react';
import Navbar from '../components/navbar';
import axios from 'axios';
import { InputForm } from '../components/MBForm';
import { Link } from 'react-router-dom';
import form from '../forms/update-credentials.json';

class UserCredentials extends Component {

  constructor (props) {
      super(props);  
      this.items = form;
      this.items.static = false;
      this.items.submit = data => this.update(data);
      this.items.cancel = () => this.onCancel();

      this.state = { form: form };

      this.onCancel = this.onCancel.bind(this);
  };

  componentDidMount () {
  };

  update (user) {
    const userId = this.props.match.params.id;
    axios.put(`/users/update/password`, user)
    .then(response => this.onCancel())
    .catch(error => this.onCancel());
  };

  onCancel () {
    this.props.history.push(`/users/${this.props.match.params.id}`);
  };

  render () {
    return (
      <div>
        <Navbar/>
        <InputForm form={this.state.form}/>
      </div>
    );
  }
};

export default UserCredentials;
