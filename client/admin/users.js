import React, { Component } from 'react';
import Navbar from '../components/navbar';
import List from '../components/List';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { loadAllUsers } from '../data';

class Users extends Component {

  constructor (props) {
    super(props);
  };

  componentDidMount () {
    if (!this.props.users || this.props.users.length === 0) {
      this.props.dispatch(loadAllUsers());
    }
  };

  render () {
    return (
      <div>
        <Navbar/>
        <div className="container mt-5">
          <div className="row justify-content-center mb-3">
            <div className="col-8">
              <Link className="btn btn-outline-primary btn-sm" role="button" to='/users/add'>Add User</Link>
            </div>
          </div>
          <div className="row justify-content-center">
            <div className="col-8">
              <List items={this.props.users}/>
            </div>
          </div>   
        </div>
      </div>
    );
  }
};

const mapStateToProps = (state) => {
  let users;
  if (state.users) {
    users = (state.users || []).map(user => {
      return { name: user.username, link: `/users/${user.id}` };
    });
  } 
    
  return { users: users };
};

export default connect(mapStateToProps)(Users);