import React, { Component } from 'react';
import Dashboard from './dashboard';
import AddUser from './add-user';
import EditUser from './edit-user';
import Users from './users';
import User from './user';
import UserCredentials from './user-credentials';
import Orgs from './orgs';
import Org from './org';
import AddOrg from './add-org';
import { loadUserData } from '../data';
import { ShowUserProfile, EditUserProfile, UpdateUserCredentials } from '../components/user-profile';
import { BrowserRouter as Router, Route, browserHistory, Switch, hashHistory } from 'react-router-dom';

class App extends Component {
  
    constructor (props) {
        super(props);
    };

    render () {   
        return (
          <Router history={browserHistory}>
            <Switch>
              <Route exact path='/' component={Dashboard} onEnter={loadUserData}/>
              <Route exact path='/users' component={Users}/>
              <Route exact path='/users/add' component={AddUser}/>
              <Route exact path='/users/:id/' component={User}/>
              <Route exact path='/users/:id/edit' component={EditUser}/>
              <Route exact path='/user/:id/profile' component={User}/>
              <Route exact path='/user/:id/profile/update' component={EditUser}/>

              <Route exact path='/user-profile' component={ShowUserProfile}/>
              <Route exact path='/user-profile/edit' component={EditUserProfile}/>
              <Route exact path='/user-profile/update-credo' component={UpdateUserCredentials}/>
              
              <Route exact path='/orgs' component={Orgs}/>
              <Route exact path='/org/add' component={AddOrg}/>
              <Route exact path='/org/:id' component={Org}/> 
            </Switch>
          </Router>
        );   
    };

};

export default App;
