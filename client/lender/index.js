import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, browserHistory, Switch, hashHistory } from 'react-router-dom';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import Main from './main';
import { ClinetList, AddClient } from './clients';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import allReducers from '../reducers';
import thunk from 'redux-thunk';
import { loadUserData } from '../data';
import ClinetDashboard from './dashboard';
import { ShowUserProfile, EditUserProfile, UpdateUserCredentials } from '../components/user-profile';
import { FromsDashboard, ShowForm } from './forms';

const store = createStore(
    allReducers,
    applyMiddleware(thunk)
);

store.dispatch(loadUserData());

class App extends Component {
    constructor (props) {
      super(props);
    };

    render () {   
        return (
          <Router history={browserHistory}>
            <Switch>
              <Route exact path='/' component={Main}/>
              <Route exact path='/user-profile' component={ShowUserProfile}/>
              <Route exact path='/user-profile/edit' component={EditUserProfile}/>
              <Route exact path='/user-profile/update-credo' component={UpdateUserCredentials}/>
              <Route exact path='/dashboard' component={ClinetDashboard}/>
              <Route exect path='/clients' component={ClinetList}/>
              <Route exect path='/clinets/add-new-client' component={AddClient}/>
              <Route exect path='/forms' component={FromsDashboard}/>
              <Route exect path='/forms/show/:id' component={ShowForm}/>
            </Switch>
          </Router>
        );   
    };
};

ReactDOM.render(
    <Provider store={store}> 
        <App/>
    </Provider>, 
    document.getElementById('root')
);
