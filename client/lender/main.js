import React, { Component } from 'react';
import Navbar from '../components/navbar';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import * as MD from 'react-icons/lib/md';

class Main extends Component {

    constructor (props) {
        super (props);
        this.state = { };
    };

    render () {

        const fontSize = 40;

        return (
            <div>
                <Navbar/>
                <div className="container">
                    <div className="row align-items-start mt-5">
                        <div className="col">
                            <div className="row justify-content-md-center justify-content-lg-center mt-2">
                                <div className="col-6 align-self-center">
                                    <Link className="list-group-item list-group-item-action border-0" to="/dashboard">
                                        <MD.MdDashboard size={fontSize} color="#666"/>
                                        <span className="align-middle ml-3 display-4">Dashboard</span>
                                    </Link>
                                </div>
                            </div>

                            <div className="row justify-content-md-center justify-content-lg-center mt-2">
                                <div className="col-6 align-self-center">
                                    <Link className="list-group-item list-group-item-action border-0" to="/clients">
                                        <MD.MdFolderShared size={fontSize} color="#666"/>
                                        <span className="align-middle ml-3 display-4">Clients</span>
                                    </Link>
                                </div>
                            </div>

                            <div className="row justify-content-md-center justify-content-lg-center mt-2">
                                <div className="col-6 align-self-center">
                                    <Link className="list-group-item list-group-item-action border-0" to="/forms">
                                        <MD.MdWork size={fontSize} color="#666"/>
                                        <span className="align-middle ml-3 display-4">Forms</span>
                                    </Link>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        );
    }
};

export default connect()(Main);