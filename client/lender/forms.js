import React, { Component } from 'react';
import { browserHistory, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Dashboard from '../components/dashboard';
import { getAllForms } from '../data';

class Show extends Component {
    constructor (props) {
        super(props);
    };

    componentDidMount () {
        if (!this.props.forms) {
            this.props.dispatch(getAllForms());
        }
    };

    render () {
        if (this.props.forms) {
            const links = this.props.forms.map(f => {
                return { name: f.name, path: `/forms/show/${f.id}` };
            });

            return (
                <Dashboard links={links}>{/* {(main())} */}</Dashboard>
            );
        }

        return (<div/>);
    };
};

/**
 * Forms
 */
class Forms extends Component {
    constructor (props) {
        super(props);
    };

    componentDidMount () {
        if (!this.props.forms) {
            this.props.dispatch(getAllForms());
        }
    };

    render () {
        if (this.props.forms) {
            const links = this.props.forms.map(f => {
                return { name: f.name, path: `/forms/show/${f.id}` };
            });

            return (
                <Dashboard links={links}>{/* {(main())} */}</Dashboard>
            );
        }

        return (<div/>);
    };
};

const mapStateToProps = state => {
    return { forms: state.forms };
};

export const FromsDashboard = connect(mapStateToProps)(Forms);
export const ShowForm = connect(mapStateToProps)(Show);

