import React, { Component } from 'react';
import Navbar from '../../components/navbar';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import * as MD from 'react-icons/lib/md';
import Dashboard from '../../components/dashboard';
import CenterPanel from '../../components/center-panel';

class Clients extends Component {

    constructor (props) {
        super (props);
        this.state = { };
    };

    render () {

        const initial = () => {
            return (
                <CenterPanel>
                    <div className="jumbotron">
                        <h1 className="display-4">You currently have no clients set up</h1>
                        <p className="lead">Start client setup process...</p>
                        <hr className="my-4"/>
                        <p className="lead">
                            <Link className="btn btn-primary mr-4" to="/clinets/add-new-client" >Add client</Link>
                            <Link className="btn btn-primary" to="/clinets/add" >Import from Salesforce</Link>
                        </p>
                    </div>
                </CenterPanel>
            );
        };

        return (
            <div>
                <Navbar/>
                {(initial())}
            </div>
            // <Dashboard>
            // </Dashboard>
        );

    };
};

export default connect()(Clients);