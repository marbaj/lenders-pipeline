import React, { Component } from 'react';
import Navbar from '../../components/navbar';
import { InputForm } from '../../components/MBForm';
import form from '../../forms/client-org.json';
import { connect } from 'react-redux';
import { createOrg } from '../../data';
import ActionPane from '../../components/action-pane';

class Add extends Component {

  constructor (props) {
      super(props);

      form.cancel = () => this.props.history.push('/clients');
      this.create = this.create.bind(this);

      form.submit = (data) => {
        this.create(data)
      }
  };

  create (data) {
    this.props.dispatch(createOrg(data))
    .then(() => {})
    .catch(() => {})
  };

  render () {
    const actions = { title: 'Add Client' };
    return (
      <div>
        <Navbar/>
        <ActionPane {...actions}/>
        <InputForm form={form}/>
      </div>
    );
  };
};

export default connect()(Add);