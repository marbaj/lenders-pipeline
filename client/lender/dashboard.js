import React, { Component } from 'react';
import { browserHistory, Link } from 'react-router-dom';
import Dashboard from '../components/dashboard';
import axios from 'axios';

class Main extends Component {

    constructor (props) {
        super(props);

        this.state = {

        };
    };

    componentDidMount () {
        axios.get('/users/info')
        .then(res => {
            this.setState({ info: res.data.info });
        })
        .catch(error => { });
    };

    render () {

        const links = [{
            name: 'Clients',
            path: '/user-list'
        },
        {
            name: 'Forms',
            path: '/orgs-list'
        }];

        const main = () => {
            if (this.state.info) {
                return (
                    <div className="jumbotron jumbotron-fluid mt-5">
                        <div className="container">
                            <h1 className="display-3">{this.state.info.org.name}</h1>
                            {/* <h1 className="display-3">Hello {this.state.user.firstName} {this.state.user.lastName}</h1> */}
                            <hr className="my-4"/>
                            <p className="lead">Welcome to Lender's Pieline</p>
                        </div>
                    </div>
                );
            } else {
                return (<div/>);
            }
        }

        return (
            <Dashboard links={links}>
                {(main())}
            </Dashboard>
        );
    }
};

export default Main;