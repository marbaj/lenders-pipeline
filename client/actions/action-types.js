
export default {
    LOGGEDIN_USER: 'LOGGEDIN_USER',
    USERS: 'USERS',
    FIND_USER: 'FIND_USER',
    ALL_ORGS: 'ALL_ORGS',
    NEW_ORG: 'NEW_ORG',
    ALL_FORMS: 'ALL_FORMS'
};