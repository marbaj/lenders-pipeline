export const loggedInUser = data => {
    return {
        type: 'LOGGEDIN_USER',
        org: data.org,
        user: data.user
    };
};

export const users = data => {
    return {
        type: 'ALL_USERS',
        users: data.users
    };
};

export const orgs = data => {
    return {
        type: 'ALL_ORGS',
        orgs: data.orgs
    };
};

export const newOrg = org => {
    return {
        type: 'NEW_ORG',
        org: org
    };
};

export const forms = data => {
    return {
        type: 'ALL_FORMS',
        forms: data.forms
    };
};

