

const applyModel = (model, items) => {
    const fn = (form, key) => { 
        if (Array.isArray(form)) {
            form.forEach(f => fn(f, key));
        } else {
            Object.keys(form).forEach(f => {
                if (Array.isArray(form[f])) {
                    fn(form[f], key);
                    form[f].forEach(f => fn(f, key));
                } else if (form.name === key) {
                    form.value = model[key];
                }
            });
        }
    };

    Object.keys(model).forEach(key => {
        fn(items, key);
    });

    return items;
};

export default applyModel;