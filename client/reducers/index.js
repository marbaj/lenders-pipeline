import { combineReducers } from 'redux';
import { userInfo, users, orgs, forms } from './reducers';

const reducers = combineReducers({
    userInfo,
    users,
    orgs,
    forms
});

export default reducers;
