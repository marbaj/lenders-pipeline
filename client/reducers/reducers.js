export const userInfo =  (state = null, action) => {
    switch (action.type) {
        case 'LOGGEDIN_USER':
            return { org: action.org, user: action.user }
            break;
        case 'UPDATE_USER':
            return { org: state.userInfo.org, user: action.user }
            break;
    }

    return state;
};

export const users = (state = null, action) => {
    switch (action.type) {
        case 'ALL_USERS':
            return action.users
            break;
    }

    return state;
};

export const orgs = (state, action) => {
    state = state || null;
    switch (action.type) {
        case 'ALL_ORGS':
            return action.orgs
            break;
        case 'NEW_ORG':
            state.push(action.org);
            return state;
            break;
        default:
            return state;
    }
};

export const forms = (state, action) => {
    state = state || null;
    switch (action.type) {
        case 'ALL_FORMS':
            return action.forms
            break;
        default:
            return state;
    }
};



