import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import Input from '../MBInput/';
import axios from 'axios';

class Login extends Component {

    constructor (props) {
        super (props);
        
        this.submit = this.submit.bind(this);
    };

    submit (data) {
        axios.post(window.location.pathname, data)
        .then(response => window.location.assign('/'))
        .catch();
    };

    render () {
        const info = { };

        const username = {
            type: 'text',
            placeholder: "Username",
            update (data) {
                info.username = data;
            }
        };

        const pass = {
            type: 'password',
            placeholder: "Password",
            update (data) {
                info.password = data;
            }
        };

        const onClick = () => {
            this.submit(info);
        };

        const divStyle = {
            "marginTop": "50px",
            "marginBottom": "20px"
        };

        return (
            <div className="container">
                <div className="row row justify-content-center">
                    <div className="col-xs-12 col-md-9" style={divStyle}>
                        <h3 className="display-3 text-center">Lender's Pipeline</h3>
                    </div>
                    <div className="col-xs-12 col-md-6" style={divStyle}>
                        <form>
                            <div className="form-group">
                                <label htmlFor="exampleInputEmail1">Username</label>
                                <Input {...username}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="exampleInputPassword1">Password</label>
                                <Input {...pass}/>
                            </div>       
                            <button type="button" className="btn btn-success" onClick={onClick}>Login</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    };
};

export default Login;