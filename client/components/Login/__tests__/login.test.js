// describe('Addition', () => {
//     it('knows that 2 and 2 make 4', () => {
//       expect(2 + 2).toBe(4);
//     });
// });


import Login from '../index';
import React from 'react';
import { mount } from 'enzyme';

test('TodoComponent renders the text inside it', () => {
   // const todo = { id: 1, done: false, name: 'Buy Milk' };
    const wrapper = mount(<Login/>);

    const p = wrapper.find('.form-group');
    expect(p.length).toBe(3);
});
