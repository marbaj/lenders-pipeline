import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import onClickOutside from 'react-onclickoutside'
import * as MD from 'react-icons/lib/md';

class ItemMenu extends Component {
    constructor (props) {
        super(props);
        this.state = { };

        this.onClick = this.onClick.bind(this);
        this.clickOutsideHandler = this.clickOutsideHandler.bind(this);
    };

    onClick () {
        this.setState({ display: this.state.display ? null : 'block' });
    };

    clickOutsideHandler (evt) {
        this.setState({ display: null });
    };

    render () {
        return (
            <div onClick={this.onClick}>
                <MD.MdAccountCircle size={40} width={40} color="#666"/>
                <div className="dropdown-menu mb-dropdown" style={this.state} aria-labelledby="navBtnSettings">
                    <Link className="dropdown-item" to={`/user-profile`}>View profile</Link>
                    <a className="dropdown-item" href="/logout">Log out</a>
                </div>
            </div>
        );
    };
};

const clickOutsideConfig = {
    handleClickOutside: (instance) => instance.clickOutsideHandler
};

const EnhancedItemMenu = onClickOutside(ItemMenu, clickOutsideConfig);

/**
 * 
 */
class Navbar extends Component {

    constructor (props) {
        super(props);
        this.state = { };
    };

    render () {
        const maintance = () => {
            const admin = true;
            if (admin) {
                return (<MD.MdSettingsApplications size={30} width={30} onClick={this.onMaintance} color="#666"/>)
            } else {
                return (<div/>);
            }
        };

        return (
            <nav className="navbar navbar-expand-md navbar-light">
                <Link className="navbar-brand" to="/">MultiFamily</Link>
                <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"/>
                </button>
                
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        {/* <li className="nav-item active ml-4">
                            <Link className="nav-link" to="/">
                                {(maintance())}
                            </Link>
                        </li> */}
                    </ul>
                    <form className="form-inline">
                        <EnhancedItemMenu/>
                    </form>
                </div>
            </nav>
        );
    };
};

const mapStateToProps = (state) => {
    if (state.userInfo) {
        const firstName =  state.userInfo.user.firstName || '';
        const lastName = state.userInfo.user.lastName || '';
        const shortName = `${firstName.substring(0, 1)}${lastName.substring(0, 1)}`;
        return {
            shortName: shortName,
            userId: state.userInfo.user.id
          };
    } else {
        return {
            shortName: '',
            userId: ''
        };
    }
};

export default connect(mapStateToProps)(Navbar);
