import React, { Component } from 'react';
import Navbar from '../components/navbar';
import { EditForm } from '../components/MBForm';
import { Link } from 'react-router-dom';
import editUserForm from '../forms/user-edit.json';
import updateCredoForm from '../forms/update-credentials.json';
import populeteForm from '../lib/populete-form';
import { connect } from 'react-redux';
import { loadAllUsers, updateUser, updateCredentials } from '../data';
import ActionPane from './action-pane';
 
const mapStateToProps = state => {
  return {
    users: state.users,
    loggedInUser: state.userInfo ? state.userInfo.user : null
  };
};

/**
 * 
 */
class ShowProfile extends Component {
  constructor (props) {
    super(props);
  };

  render () {
    const action = {
      title: 'User Preferences',
      buttons: [{
        name: 'Edit',
        link: 'user-profile/edit'
      },
      {
        name: 'Update credentials',
        link: 'user-profile/update-credo'
      }]
    };

    const user = this.props.loggedInUser || {};

    return (
      <div>
        <Navbar/>
        <ActionPane {...action}/>
        <div className="container">
          <div className="row">
            <div className="col-3"><p className="text-right">Name:</p></div>
            <div className="col-3">{user.firstName} {user.lastName}</div>
          </div>
          <div className="row">
            <div className="col-3"><p className="text-right">Email:</p></div>
            <div className="col-3">{user.email}</div>
          </div>
        </div>
      </div>
    );
  };
};

class UpdateCredentials extends Component {

  constructor (props) {
      super(props); 
      
      this.state = { };
      this.onUpdate = this.onUpdate.bind(this);
      this.onCancel = this.onCancel.bind(this);

      updateCredoForm.submit = this.onUpdate; 
      updateCredoForm.cancel = this.onCancel;
  };

  onUpdate (data) {
    updateCredentials(data)
    .then(this.onCancel)
    .catch(this.onCancel);
  };

  onCancel () {
    this.props.history.push('/user-profile');
  };

  render () {
    const action = {
      title: 'User Preferences'
    };

    return (
      <div>
        <Navbar/>
        <ActionPane {...action}/>
        <div className="row justify-content-md-center">
            <div className="col-6">
            <EditForm {...updateCredoForm}/>
            </div>
        </div>
      </div>
    );
  };

};

/**
 * 
 */
class EditProfile extends Component {

  constructor (props) {
      super(props); 
      
      this.state = { };
      this.onUpdate = this.onUpdate.bind(this);
      this.onCancel = this.onCancel.bind(this);
  };

  componentDidMount () {
    if (!this.props.users || this.props.users.length === 0) {
      this.props.dispatch(loadAllUsers());
    }
  };

  onUpdate (user) {
    this.props.dispatch(updateUser(user))
    .then(res => this.onCancel())
    .catch(err => this.onCancel())
  };

  onCancel () {
    this.props.history.push('/user-profile');
  };

  render () {
    editUserForm.cancel = this.onCancel;
    editUserForm.submit = this.onUpdate; 
    const form = this.props.loggedInUser ? populeteForm(this.props.loggedInUser, editUserForm) : null;

    const action = {
      title: 'User Preferences',
    };

    const edit = () => {
      if (this.props.loggedInUser) {
        return (
          <div className="row justify-content-md-center">
             <div className="col-6">
              <EditForm {...form}/>
             </div>
          </div>
        );
      } else {
        return (<div/>);
      }
    };

    return (
      <div>
        <Navbar/>
        <ActionPane {...action}/>
        {(edit())}
      </div>
    );
  }
};

export const ShowUserProfile = connect(mapStateToProps)(ShowProfile);
export const EditUserProfile = connect(mapStateToProps)(EditProfile);
export const UpdateUserCredentials = connect(mapStateToProps)(UpdateCredentials);
