import React, { Component } from 'react';
import Navbar from '../components/navbar';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

class ActionPane extends Component {

    constructor (props) {
        super(props);  
    };

    render () {

        const buttons = (this.props.buttons || []).map((button, index) => {
            if (button.link) {
                return (
                    <Link className="btn btn-outline-primary btn-sm ml-3" key={index} to={button.link}>{button.name}</Link> 
                );
            } else {
                return (
                    <button className="btn btn-outline-primary btn-sm ml-3" key={index} onClick={button.onClick}>{button.name}</button> 
                );
            }
        });

        return (
            <div className="container mt-3 mb-4">
                <div className="row justify-content-between">
                    <div className="col-4">
                        <p className="display-4">{this.props.title}</p>
                    </div>
                    <div className="col-4">{buttons}</div>
                </div>
            </div>
        );
    }
};

export default connect()(ActionPane);
