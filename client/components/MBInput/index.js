import React from 'react';
import ReactDOM from 'react-dom';

class Input extends React.Component {
    constructor (props) {
        super(props);
        
        this.state = { value: props.value || '' };
        this.onChange = this.onChange.bind(this);
    };

    onChange (event) {
        this.setState( { value: event.target.value });
        if (this.props.update) {
            this.props.update(event.target.value);
        } 
    };

    render () {
        const input = () => {
            return (
                //display-4 border border-top-0 border-right-0 border-left-0 border-primary
                <input className="form-control"
                    type={this.props.type} 
                    placeholder={this.props.placeholder}
                    value={this.state.value} 
                    onChange={this.onChange} 
                />                     
            );
        };

        const label = () => {
            //h4 className="display-4 border border-top-0 border-right-0 border-left-0 border-primary"
            return (<label>{this.props.value}</label>);
        };

        if (this.props.static) {
            return (<span className="form-static-label">{(label())}</span>);
        } else {
            return (<div>{(input())}</div>);
        }                
    };
};

export default Input;
