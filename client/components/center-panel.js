import React, { Component } from 'react';

class CenterPanel extends Component { 
    constructor (props) {
        super (props);
    };

    render () {
        const marginTop = this.props.marginTop || 5;
        const cols = this.props.cols || 8;

        return (
            <div className="container">
                <div className={`row justify-content-md-center justify-content-lg-center mt-${marginTop}`}>
                    <div className={`col-${cols}`}>
                        {this.props.children}
                    </div>
                </div>
            </div>    
        );
    };
};

export default CenterPanel;
    

