import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class List extends Component {

    constructor (props) {
        super(props);
    };

    render () {
        const list = () => {
            return (
                <ul className="list-group">
                    {this.props.items.map((item, index) => {
                        return (
                            <Link className="list-group-item mt-1 border border-top-0 border-right-0 border-left-0 border-primary" key={index} role="link" to={item.link}>
                                {item.name}
                            </Link>  
                        );
                    })}
                </ul>
            );
        };

        if (this.props.items && this.props.items.length > 0) {
            return (<div>{list()}</div>);
        } else {
            return (<div/>);
        }
    };
};

export default List;