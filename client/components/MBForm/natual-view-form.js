
import React from 'react';
import ReactDOM from 'react-dom';

class NatualViewForm extends React.Component {

    constructor (props) {
        super(props);
    };

    render () {

        const inlineItems = items => {
            const r = 12 / items.length;
            return (items.fields || []).map((item, index) => {
                // const mdSize = item.size ? item.size.md : r;
                // const smSize = item.size ? item.size.sm : r;    
                return <div className="col-md-auto col-sm-auto pl-0 pr-2" key={index.toString()}>{item.value}</div>;
            });
        }; 
    
        if (!this.props.form) {
            return (<div/>);
        }

        const form = () => {
            return (this.props.form.inputs || []).map((item, index) => {
                return (
                    <div className="row justify-content-md-start justify-content-sm-start mb-2" key={index.toString()}>
                        {(() => {
                            return item.label ? <div className="col-md-3 col-sm-3 text-left pl-0">{item.label}</div> : ''
                        })()}
        
                        {(inlineItems(item))}
                    </div>
                );
            });
        }

        return (<div>{(form())}</div>);
    };
};


export default NatualViewForm;

