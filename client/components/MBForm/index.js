
import Edit_Form from './edit-form.js';
import View_Form from './view-form.js';
import React, { Component } from 'react';
//import populeteForm from '../lib/populete-form';

export const InputForm = class extends Component {
    constructor (props) {
        super(props);  
    };

    render () {
        return (
            <div className="container">
                <div className="row justify-content-center mb-3 pl-3 pr-3">
                    <div className="col-8">{this.props.children}</div>
                </div>
                {/* <div className="row justify-content-center mb-3">  
                </div>  */}
                <div className="row justify-content-center">
                    <div className="col-8">
                        <Edit_Form {...this.props.form}/> 
                    </div>
                </div>
            </div>
        );
    };
};
 
export const EditForm = Edit_Form;
export const ViewForm = View_Form;

