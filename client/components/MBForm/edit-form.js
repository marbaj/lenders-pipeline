import React from 'react';
import ReactDOM from 'react-dom';
import MBInput from '../MBInput/';
import MBButton from '../MBButton/';

class Form extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
            submitDisabled: true
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.onCancel = this.onCancel.bind(this);
    };

    _update (control) {
        return data => this.update(data, control)
    };

    /**
     * Initialize value for all inputs
     */
    componentDidMount () {
        (this.props.inputs || []).forEach(item => {
            if (Array.isArray(item)) {
                item.forEach(i => this.setState({ [i.name]: i.value }));
            } else {
                this.setState({ [item.name]: item.value });
            }
        });
    };

    /**
     * Not implemented
     */
    shouldComponentUpdate () {
        return true;
    };

    validation (control, data) {
        const ctrl = this.props.inputs.find(el => {
            return Array.isArray(el) ? el.find(e => e.name === control) : el.name === control;
        });

        const validations = ctrl.validations || { };
        const hasChanged = ctrl.value !== data;
       
        if (hasChanged === false && validations.sameString !== false) {
            return false;
        } else if (validations.fn) {
            return validations.fn(data);
        }

        return true;
    };

    update (data, control) {
        this.setState({ [control]: data });
        const flag = !this.validation(control, data);
        
        this.setState({ submitDisabled: flag });
    };

    handleSubmit () {
        const data = { };
 
        (this.props.inputs || []).forEach(item => {
            if (Array.isArray(item)) {
                item.forEach(i => data[i.name] = this.state[i.name]);
            } else {
                data[item.name] = this.state[item.name]
            }
        });

        this.props.submit(data);
    };

    onCancel () {
        this.props.cancel();
    };

    render () {
        const divStyle = {
            paddingLeft: 0, 
            paddingRight: 0
        };

        const formItem = (item, index) => {
            return (
                <div className="form-group" key={item}>
                    <label className="col-sm-12 col-md-12" htmlFor={`formHorizontal_${item.name}`}>{item.label}</label>
                    <div className="col-sm-12 col-md-12">
                        <MBInput {...item} static={this.props.static} id={`formHorizontal_${item.name}`} update={this._update(item.name)}/>
                    </div>
                </div>
            );
        };

        const inlineItems = items => {
            const r = 12 / items.length;
            return items.map((item, index) => {
                const mdSize = item.size ? item.size.md : r;
                const smSize = item.size ? item.size.sm : r;
                if (item.type === 'filler') {
                    return (<div className={`col-sm-${smSize} col-md-${mdSize}`} style={divStyle} key={index}/>);                    
                } else {
                    return (<div className={`col-sm-${smSize} col-md-${mdSize}`} style={divStyle} key={index}>{(formItem(item,index))}</div>);    
                }
            });
        };

        const stackedItems = (item, index) => {
            const mdSize = item.size ? item.size.md : 6;
            const smSize = item.size ? item.size.sm : 6;
            return (<div className={`col-sm-${smSize} col-md-${mdSize}`} style={divStyle} key={index}>{(formItem(item,index))}</div>);
        };

        const form = () => {
            return (this.props.inputs || []).map((item, index) => {
                if (Array.isArray(item)) {
                    return (<div className="row" key={index.toString()}>{(inlineItems(item, index))}</div>);
                } else {
                    return (<div className="row" key={index.toString()}>{(stackedItems(item, index))}</div>);
                }
            });
        };

        const size = this.props.size || {};
        size.sm = size.sm || 12;
        size.md = size.md || 12;

        const doneButton = {
            title: this.props.submitTitle || 'Done',
            onClick: this.handleSubmit,
            hide: this.props.static,
            disabled: this.state.submitDisabled
        };

        const cancelButton = {
            title: this.props.cancelTitle || 'Cancel',
            onClick: this.onCancel,
            hide: this.props.static,
            disabled: this.state.cancelDisabled
        };

        return (  
            <div className="row justify-content-md-center">
                <div className={`col-sm-${size.sm} col-md-${size.md}`}>           
                    {(form())}
                    <div className="row">
                        <div className="col-md-12 col-sm-12">
                            <div className="float-right ml-4">  
                                <MBButton {...doneButton}/>
                            </div>
                            <div className="float-right ml-4">   
                                <MBButton {...cancelButton}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    };
};

export default Form;