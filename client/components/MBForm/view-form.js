import React from 'react';
import ReactDOM from 'react-dom';
import MBInput from '../MBInput/';

import NatualViewForm from './natual-view-form';

class Form extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
            submitDisabled: true
        };
    };

        /**
     * Initialize value for all inputs
     */
    componentDidMount () {
        (this.props.inputs || []).forEach(item => {
            if (Array.isArray(item)) {
                item.forEach(i => this.setState({ [i.name]: i.value }));
            } else {
                this.setState({ [item.name]: item.value });
            }
        });
    };

    _update (control) {
        return data => this.update(data, control)
    };

    render () {
        const size = this.props.size || {};
        size.sm = size.sm || 12;
        size.md = size.md || 12;

        return (
            <div className="container">
                <div className="row justify-content-md-center mt-4">
                <div className="col-md-8">{this.props.children}</div>
                </div> 
                <div className="row justify-content-md-center justify-content-sm-center mt-4">
                    <div className="col-md-8"><NatualViewForm {...this.props}/></div>
                </div>
            </div>
        );
    };
};

export default Form;