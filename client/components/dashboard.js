import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Navbar from './navbar';

class Dashboard extends Component {

  constructor (props) {
      super(props);

      this.state = {
      };
  };

  componentDidMount () {

  };

  render () {

    const links = () => {
        return (this.props.links || []).map((link, index) => {
            return (
                <li className="nav-item" key={index}>
                    <Link className="btn btn-link" role="button" to={link.path}>{link.name}</Link>
                </li>
            );
        });
    };

    const sideBar = () => {
      return(
        <div className="sidebar">
          <ul className="nav flex-column">{(links())}</ul>
        </div>
      );
    };

    const main = () => {
      return(
        <div className="container">
          <div className="row">
            <div className="col-sm-12 col-md-12"> 
                <div>{this.props.children}</div>
            </div>
          </div>
        </div>
      );
    };

    return (
      <div>
        <Navbar/>
          {(sideBar())}
          <div className="main">
            {(main())}
          </div>
      </div>
    );
  }
};

export default Dashboard;