import React from 'react';
import ReactDOM from 'react-dom';

class MBForm extends React.Component {
    constructor (props) {
        super(props);
    };

    render () {
        if (this.props.hide) {
            return (<div/>);
        }
        
        if (this.props.disabled) {
            return (
                <button type="button" className="btn btn-outline-primary" disabled>{this.props.title}</button>
            );
        } else {
            return (
                <button type="button" className="btn btn-outline-primary" onClick={this.props.onClick}>{this.props.title}</button>
            );
        }
    };
};

export default MBForm;