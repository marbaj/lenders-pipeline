import axios from 'axios';
import { loggedInUser, users, orgs, forms } from '../actions';

export const loadUserData = () => {
     return (dispatch) => {
         return axios.get('/users/info')
            .then(res => {
                dispatch(loggedInUser({ type: 'LOGGEDIN_USER', org: res.data.info.org, user: res.data.info.user }));
                return res;
            })
            .catch (err => err);
     };
};

export const updateUser = (user) => {
    return (dispatch) => {
        return axios.put('/users/update', user)
            .then(res => {
                dispatch(loggedInUser({ type: 'LOGGEDIN_USER', org: res.data.info.org, user: res.data.info.user }));
                return res;
            })
            .catch (err => err);
    };
};

export const updateCredentials = user => {
    return axios.put('/users/update/password', user);
};

export const loadAllUsers = () => {
    return (dispatch) => {
        return axios.get('/users/get-all')
            .then(res => {
                return dispatch(users({ type: 'ALL_USERS', users: res.data.users }));
                return res;
            })
            .catch (err => err);
    };
};

export const loadAllOrgs = () => {
    return (dispatch) => {
        return axios.get('/org/all')
            .then(res => {
                dispatch(orgs({ type: 'ALL_ORGS', orgs: res.data.orgs }));
                return res;
            })
            .catch (err => err);
    };
};

export const createOrg = (org) => {
    return (dispatch) => {
        return axios.post('/org/create/clinet-org', org)
            .then(res => {
                dispatch(orgs({ type: 'NEW_ORG', org: res.data.org }));
                return res;
            })
            .catch (err => err);
    };
};

export const getAllForms = () => {
    return (dispatch) => {
        return axios.get('/forms/get/')
            .then(res => {
                dispatch(forms({ type: 'ALL_FORMS', forms: res.data.forms }));
                return res;
            })
            .catch (err => err);
    };
};
