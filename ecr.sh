#!/bin/bash

#aws ecr get-login --no-include-email --region us-east-2
eval "$(aws ecr get-login --no-include-email --region us-east-2)"

docker build -t lender-pipeline .
docker tag lender-pipeline:latest 676532431744.dkr.ecr.us-east-2.amazonaws.com/lender-pipeline:latest
docker push 676532431744.dkr.ecr.us-east-2.amazonaws.com/lender-pipeline:latest

